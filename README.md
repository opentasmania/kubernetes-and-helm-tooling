# Kubernetes And Helm Tooling

## Installers

A number of installers can be found in the [installers](installers) directory.

## Manifests

A collection of manifests can be found in the [manifests](manifests) directory.

## Roadmap
More manifests for things

## Contributing
Open for [corrections and contributions](https://gitlab.com/opentasmania/kubernetes-and-helm-tooling/-/issues).

## Authors and acknowledgment
Peter Lawler

## License
This is an [MIT Licenced](LICENSE) project.
## Project status
Randomly updated.
