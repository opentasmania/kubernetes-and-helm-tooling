#!/usr/bin/env python
# MIT License
#
# Copyright (c) 2024 Peter Lawler <relwalretep@plnom.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
from os.path import isfile
from logging import INFO, Formatter, StreamHandler, getLogger
from subprocess import DEVNULL, STDOUT, CalledProcessError, run
from subprocess import PIPE, Popen
from sys import stdout
from urllib.request import urlopen

logger = getLogger('kubernetes-and-helm-tooling.helm-install')
logger.setLevel(INFO)
handler = StreamHandler(stdout)
formatter = Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)


def run_command(command):
    process = Popen(command, stdout=PIPE, stderr=STDOUT, text=True, shell=True)
    return process.communicate()[0]


try:
    run('sudo --validate --no-update', shell=True, check=True)
    logger.info('Has sudo access')
except CalledProcessError:
    logger.info('No sudo access')
    exit(1)

if not isfile("/usr/local/bin/helm"):
    asc_file = urlopen("https://baltocdn.com/helm/signing.asc").read()
    with open("/etc/apt/keyrings/helm.gpg", "wb") as f:
        f.write(asc_file)

    logger.info(run_command(
        'echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/helm.gpg] '
        'https://baltocdn.com/helm/stable/debian/ all main" | sudo tee '
        '/etc/apt/sources.list.d/helm-stable-debian.list'))
    logger.info(run_command('sudo apt update'))
    logger.info(run_command('sudo apt install helm'))

logger.info(run_command('helm version'))
