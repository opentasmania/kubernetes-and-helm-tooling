## [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/)

A [kubectl install](kubectl-install.py) script for linux systems.

## [minikube](https://minikube.sigs.k8s.io/)

A [minikube install](minikube-install.py) script for linux systems.

## [Helm](https://helm.sh)
A [helm install](helm-install.py) script for linux systems.