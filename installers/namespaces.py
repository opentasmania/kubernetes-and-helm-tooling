#!/usr/bin/env python
# MIT License
#
# Copyright (c) 2024 Peter Lawler <relwalretep@plnom.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from logging import INFO, Formatter, StreamHandler, getLogger
from subprocess import run
from sys import stdout

logger = getLogger('kubernetes-and-helm-tooling.namespaces')
logger.setLevel(INFO)
handler = StreamHandler(stdout)
formatter = Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
namespaces = ['dev', 'prod']

logger.info(f"Current namespace:")
run("kubectl get namespaces --show-labels", shell=True, check=True)

for f in namespaces:
    # TODO: Ensure namespace doesn't already exist
    logger.info(f"Installing namespace: {f}")
    run(f"kubectl create -f manifests/namespaces/namespace-{f}.yaml", shell=True, check=True)
    run("kubectl get namespaces --show-labels", shell=True, check=True)
