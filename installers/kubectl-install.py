#!/usr/bin/env python
# MIT License
#
# Copyright (c) 2024 Peter Lawler <relwalretep@plnom.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
import subprocess
from logging import INFO, Formatter, StreamHandler, getLogger
from os import chmod, lstat, makedirs
from os.path import isdir
from os.path import isfile
from stat import S_IMODE
from subprocess import CalledProcessError
from subprocess import Popen, run
from sys import stdout

from requests import get

logger = getLogger('kubernetes-and-helm-tooling.kubectl-install')
logger.setLevel(INFO)
handler = StreamHandler(stdout)
formatter = Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)


def has_sudo_access():
    try:
        run('sudo --validate --no-update', shell=True, check=True)
        logger.info('Has sudo access')
    except CalledProcessError:
        logger.info('No sudo access')
        exit(1)


def file_exists(path):
    return isfile(path)


def directory_exists(path):
    return isdir(path)


def create_directory(path):
    try:
        makedirs(path, mode=429, exist_ok=False)
        return True
    except OSError:
        return False


def chmod(path, permission):
    try:
        chmod(path, permission)
        return True
    except OSError:
        return False


def get_directory_permission(path):
    return S_IMODE(lstat(path).st_mode)


def install_kubectl():
    if has_sudo_access() and not file_exists("/usr/bin/kubectl"):
        # If the folder `/etc/apt/keyrings` does not exist, it should be created before the curl command,
        # read the note below.
        keyrings_dir = "/etc/apt/keyrings"
        if not directory_exists(keyrings_dir):
            if not create_directory(keyrings_dir):
                exit(1)
        else:
            if get_directory_permission(keyrings_dir) != 755:
                if not chmod(keyrings_dir, 755):
                    exit(1)
        r = get('https://cdn.dl.k8s.io/release/stable.txt')
        r.raise_for_status()
        version = r.text.strip()

        commands = """
            curl --fail --silent --show-error --location https://pkgs.k8s.io/core:/stable:/v1.30/deb/Release.key | sudo gpg --dearmor --output /etc/apt/keyrings/kubernetes-apt-keyring.gpg
            sudo chmod 644 /etc/apt/keyrings/kubernetes-apt-keyring.gpg
            echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.30/deb/ /' | sudo tee /etc/apt/sources.list.d/kubernetes.list
            sudo chmod 644 /etc/apt/sources.list.d/kubernetes.list
            sudo apt update
            sudo apt install --yes kubectl
        """

        process = Popen('/bin/bash', stdin=subprocess.PIPE, stdout=subprocess.PIPE)
        out, err = process.communicate(commands.encode('utf-8'))
        logger.info(out.decode('utf-8'))  # Output from the shell commands


install_kubectl()
