#!/usr/bin/env python
# MIT License
#
# Copyright (c) 2024 Peter Lawler <relwalretep@plnom.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
from logging import INFO, Formatter, StreamHandler, getLogger
from os.path import isfile
from random import choice
from re import match
from subprocess import CalledProcessError, run
from sys import stdout

logger = getLogger('kubernetes-and-helm-tooling.minikube-install')
logger.setLevel(INFO)
handler = StreamHandler(stdout)
formatter = Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

try:
    run('sudo --validate --no-update', shell=True, check=True)
    logger.info('Has sudo access')
except CalledProcessError:
    logger.info('No sudo access')
    exit(1)

namespace = "development"
node_count = 3

# TODO: ensure the name is not currently in use
with open('/usr/share/dict/words', 'r') as f:
    words = [word.strip().lower() for word in f if len(word.strip()) == 8 and match("^[a-zA-Z]*$", word.strip())]
    profile_name = choice(words)
    logger.info(f"Selected '{profile_name}' for profile name")

if not isfile("/usr/bin/minikube"):
    run('sudo apt update', shell=True, check=True)
    run(
        'curl --location --remote-name https://storage.googleapis.com/minikube/releases/latest/minikube_latest_amd64.deb',
        shell=True, check=True)
    run('sudo apt --yes install ./minikube_latest_amd64.deb', shell=True, check=True)

run('minikube version', shell=True, check=True)

run('minikube config -p kubevirt set vm-driver kvm2', shell=True, check=True)
run('minikube config -p kubevirt set cpus 2', shell=True, check=True)
run('minikube config -p kubevirt set memory 4096', shell=True, check=True)
run('minikube config -p kubevirt set disk-size 32768', shell=True, check=True)

logger.info(f"Attempting to create cluster '{profile_name}' with '{namespace}' namespace")
run(f'minikube start --namespace={namespace} --profile={profile_name} --ha=true --output=text '
    f'--nodes={node_count} --cni=flannel --extra-disks=1 --wait=all --embed-certs --install-addons',
    shell=True, check=True)

run(f'minikube profile list {profile_name}', shell=True, check=True)
run(f'minikube status --profile {profile_name}', shell=True, check=True)

run(f'sudo virsh autostart {profile_name} && sudo virsh autostart {profile_name}-m02 && sudo virsh '
    f'autostart {profile_name}-m03', shell=True, check=True)

essential_addons = ["dashboard", "default-storageclass", "metrics-server", "registry", "volumesnapshots"]
optional_addons = ["auto-pause"]
thirdparty_minikube_kaddons = ["helm-tiller", "logviewer", "yakd"]
mkaddons = essential_addons + optional_addons + thirdparty_minikube_kaddons

for f in mkaddons:
    user_input = input(f"Do you want to install add-on {f}? (y/N): ").strip().lower()
    user_input = user_input[0] if user_input else 'n'
    if user_input == 'y':
        run(f'minikube --profile {profile_name} addons enable {f}', shell=True, check=True)
    else:
        logger.info(f"Skipping installation of add-on {f}")

run(f'minikube --profile {profile_name} dashboard --url &', shell=True, check=True)
run(f'minikube --profile {profile_name} service yakd-dashboard -n yakd-dashboard', shell=True, check=True)
