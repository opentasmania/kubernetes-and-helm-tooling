#!/usr/bin/env python
# MIT License
#
# Copyright (c) 2024 Peter Lawler <relwalretep@plnom.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
from logging import INFO, Formatter, StreamHandler, getLogger
from os import remove
from os.path import expanduser, isdir, normpath
from os.path import isfile
from shutil import rmtree
from subprocess import DEVNULL, STDOUT, run
from subprocess import check_output
from sys import stdout

logger = getLogger('kubernetes-and-helm-tooling.cleanup')
logger.setLevel(INFO)
handler = StreamHandler(stdout)
formatter = Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

minikube_files_and_dirs = ("addons bin ca.crt ca.key ca.pem cert.pem certs config files key.pem logs machines "
                           "machine.lock profiles proxy-client-ca.crt proxy-client-ca.key").split()
minikube_profiles = check_output(
    "minikube profile list --light |grep -v '\\-\\-\\-'|grep -v 'Kubecontext'|sed s/'|'//g|awk '{print $1}'",
    shell=True).decode().split("\n")

logger.info("Stopping all minikubes")
run(['minikube stop --all'], shell=True, check=True)

if minikube_profiles:
    for f in [profile for profile in minikube_profiles if profile]:
        logger.info(f"Removing minikube (profile {f})")
        run(["minikube delete --profile {}".format(f)], shell=True, check=True)
else:
    logger.info("No minikube profiles found")

for f in minikube_files_and_dirs:
    g = normpath(expanduser("~/.minikube/{}".format(f)))
    if isfile(g):
        logger.info(f"Removing minikube user file: {g}")
        remove(g)
    if isdir(g):
        logger.info(f"Removing minikube user dir: {g}")
        rmtree(g, ignore_errors=False)

if isfile(expanduser("~/.kube/config")):
    logger.info("Removing kube user config")
    remove(expanduser("~/.kube/config"))
else:
    logger.info("No kube user config file found")

if isfile(expanduser("~/.helm")):
    logger.info("Removing helm user directory")
    rmtree(expanduser("~/.helm"), ignore_errors=True)
else:
    logger.info("No helm user files found")

logger.info("Restarting containerd")
run("sudo systemctl restart containerd", shell=True, stdout=DEVNULL, stderr=STDOUT)
logger.info("Restarting docker")
run("sudo systemctl restart docker", shell=True, stdout=DEVNULL, stderr=STDOUT)
