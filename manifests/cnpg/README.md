
#### [Cloud Native Postgres Operator](https://cloudnative-pg.io/)

##### Install CNPG Operator
```bash
helm repo add cnpg https://cloudnative-pg.github.io/charts
helm upgrade --install cnpg \
  --namespace cnpg-system \
  --create-namespace \
  cnpg/cloudnative-pg
```
then create the cluster
```bash
kubectl apply --server-side -f manifests/cnpg/cnpg-claim.yaml
kubectl apply --server-side -f manifests/cnpg/cnpg-configmap.yaml
kubectl apply --server-side -f manifests/cnpg/cnpg-deployment.yaml
kubectl apply --server-side -f manifests/cnpg/cnpg-pv.yaml
kubectl apply --server-side -f manifests/cnpg/cnpg-service.yaml
kubectl apply --server-side -f manifests/cnpg/cnpg-stateful-set.yaml
kubectl cnpg status cnpg
```